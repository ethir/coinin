import React, { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';

import store from './store';
const history = createBrowserHistory();

import Layout from './layout';
import Posts from './containers/Posts';
import Detail from './containers/detail';
import Comments from './containers/comments';

render(
    <Provider store={store}>
        <BrowserRouter>
            <Router history={history}>
                <div>
                    <Route component={Layout} />
                    <Route exact path='/' component={Posts} />
                    <Route exact path='/posts/:id' component={Detail} />
                </div>
            </Router>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
