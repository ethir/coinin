import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

import { fetchPosts } from '../actions/posts';
import TagsList  from './tags';

class PostsList extends Component {
    componentWillMount() {
        this.props.fetchPosts();
    }
    render() {
        return (
	        <div className="container-fluid">
		        <div className="row">
			        {
				        <div className="col-md-9">
					        <div className="row">
						        {this.props.posts.items.map(post => (
							        <div key={post.id} className="col-md-4">
								        <article className="card mb-4">
									        <Link  to={`/posts/${post.id}`}>
										        <img className="card-img" src={post.image} alt="" />
									        </Link>
									        <div className="card-body">
                                                <Link  to={`/posts/${post.id}`}>
                                                    <h4 className="card-title">{post.title}</h4>
                                                </Link>
                                                <div className="card-meta">
                                                    <a href="#">
                                                        <time className="timeago" dateTime="2017-10-15 20:00" data-tid="9">5
                                                            months ago
                                                        </time>
                                                    </a> in <a href="#">Lifestyle</a>
                                                </div>
										        <p className="card-text">{post.body}</p>
									        </div>
								        </article>
							        </div>
						        ))}
					        </div>
				        </div>
			        }
			        <div className="col-md-3 ml-auto">
				        <aside className="sidebar">
					        <div className="card mb-4">
						        <div className="card-body">
							        <h4 className="card-title">About</h4>
							        <p className="card-text">Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam <a href="#">semper libero</a>, sit amet adipiscing sem neque sed ipsum. </p>
						        </div>
					        </div>
				        </aside>

				        <aside className="sidebar sidebar-sticky">
					        <div className="card mb-4">
						        <TagsList />
					        </div>
					        <div className="card mb-4">
						        <div className="card-body">
							        <h4 className="card-title">Popular stories</h4>

							        <a href="post-image.html" className="d-inline-block">
								        <h4 className="h6">The blind man</h4>
								        <img className="card-img" src="img/2.jpg" alt="" />
							        </a>
							        <time className="timeago" dateTime="2017-10-03 20:00" data-tid="14">6 months ago</time> in Lifestyle

							        <a href="post-image.html" className="d-inline-block mt-3">
								        <h4 className="h6">Crying on the news</h4>
								        <img className="card-img" src="img/3.jpg" alt="" />
							        </a>
							        <time className="timeago" dateTime="2017-07-16 20:00" data-tid="15">8 months ago</time> in Work

						        </div>
					        </div>
				        </aside>
			        </div>
		        </div>
	        </div>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts
});

export default connect(mapStateToProps, { fetchPosts })(PostsList);
