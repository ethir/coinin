import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

import { fetchTags } from '../actions/tags';


class TagsList extends Component {
    componentWillMount() {
        this.props.fetchTags();
    }
    render() {
        return (
		        <div className="card mb-4">
			        <div className="card-body">
				        <h4 className="card-title">Tags</h4>
				        {
				        	this.props.tags.items.map((tag) => (
								<a key={tag.id} className="btn btn-light btn-sm mb-1" href="#">{tag.name}</a>
				        	))
				        }
			        </div>
		        </div>
        );
    }
}

const mapStateToProps = state => ({
    tags: state.tags
});

export default connect(mapStateToProps, { fetchTags })(TagsList);
