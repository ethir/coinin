import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchPost } from "../actions/post";

import Comments from './comments';

class Detail extends Component {
    componentWillMount() {
        this.props.fetchPost(this.props.match.params.id);
    }

    render() {
        console.log(this.props);
        return (
            <div className="col-md-9">
                <article className="card mb-4">
                    <header className="card-header text-center">
                        <div className="card-mPeta">
                            <a href="#">
                                <time className="timeago" dateTime="2017-10-26 20:00" data-tid="6">5 months ago</time>
                            </a> in <a href="#">Journey</a>
                        </div>
                        <a href="post-image.html">
                            <h1 className="card-title">{this.props.post.title}</h1>
                        </a>
                    </header>
                    <a>
                        <img className="card-img" src={this.props.post.image} alt="" />
                    </a>
                    <div className="card-body">
                        {this.props.post.body}
                        <Comments id={this.props.match.params.id} />
                    </div>
                </article>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    post: state.posts.item
});

export default connect(mapStateToProps, { fetchPost })(Detail);