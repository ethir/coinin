import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchComments } from "../actions/comments";

class Comments extends Component {
    componentWillMount() {
        this.props.fetchComments(this.props.id);
    }

    render() {
        return (
            <div>
                <h3>Comments</h3>
                {
                    this.props.comments.items.map(comment => (
                        <div key={comment.id} className="media mb-3">
                            <div className="text-center">
                                <img className="mr-3 rounded-circle" src="img/avatars/3.png" alt="Lucy" width="100"
                                     height="100" />
                                <h6 className="mt-1 mb-0 mr-3">Lucy</h6>
                            </div>
                            <div className="media-body">
                                <p className="mt-3 mb-2">{comment.body}</p>
                                <time className="timeago text-muted" dateTime="2017-12-03 20:00" data-tid="10">3 months ago</time>
                            </div>
                        </div>
                    ))
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    comments: state.comments
});

export default connect(mapStateToProps, { fetchComments })(Comments);