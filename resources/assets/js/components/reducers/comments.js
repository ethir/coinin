import { FETCH_COMMENTS } from '../actions/types';

const initialSate = {
    items: []
};

export default function(state = initialSate, action)  {
    switch (action.type) {
        case FETCH_COMMENTS:
            return {
                ...state,
                items: action.payload
            };
        default:
            return state;
    }
}