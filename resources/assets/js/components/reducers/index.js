import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import postReducers from './posts';
import commentsReducers from './comments';
import tagsReducer from './tags';

export default combineReducers({
    router: routerReducer,
    posts: postReducers,
    comments: commentsReducers,
    tags: tagsReducer
})