import { FETCH_POSTS, NEW_POST } from '../actions/types';
import axios from 'axios';

export const fetchPosts = () => dispatch => {
    axios.get('/api/posts').then(posts => dispatch({
        type: FETCH_POSTS,
        payload: posts.data,
    }))
};