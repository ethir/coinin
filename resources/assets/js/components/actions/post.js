import { FETCH_POST } from '../actions/types';
import axios from 'axios';

export const fetchPost = (id) => dispatch => {
    axios.get(`/api/posts/${id}`).then(post => dispatch({
        type: FETCH_POST,
        payload: post.data,
    }))
};