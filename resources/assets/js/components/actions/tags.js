import { FETCH_TAGS } from './types';
import axios from 'axios';

export const fetchTags = () => dispatch => {
    axios.get('/api/tags').then(tags => dispatch({
        type: FETCH_TAGS,
        payload: tags.data,
    }));
};