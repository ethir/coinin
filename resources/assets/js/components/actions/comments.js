import { FETCH_COMMENTS } from '../actions/types';
import axios from 'axios';

export const fetchComments = (id) => dispatch => {
    axios.get(`/api/posts/${id}/comments`).then(post => dispatch({
        type: FETCH_COMMENTS,
        payload: post.data,
    }))
};