@extends('layouts.main')

@section('content')
	<div class="col-md-8">
		<h1>Sign in qweqwe</h1>
		
		<form method="post" action="/login">
			{{ csrf_field() }}

			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Login</button>	
			</div>

			@include('layouts.errors')

		</form>
	</div>


@endsection