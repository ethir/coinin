@extends ('layouts.main')

@section('title')
	Post Creation
@endsection

@section('content')	
	<form method="POST" action="/posts" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
			<label for="postTitle">Title</label>
			<input type="text" class="form-control" name="title" placeholder="Enter Title Here">
		</div>

		<div class="form-group">
			<textarea class="form-control" name="body" rows="3"></textarea>
		</div>
		
		<div class="form-group">
                    <label for="postTags">Tags</label>
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		    <input type="text" class="form-control" name="tags[]" placeholder="Enter tag Here">
		</div>

		<div class="form-group">
			<input type="file" name="image">
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary">Publish</button>
		</div>
	</form>

	{{-- Сохранение файла - вернет строку с путем --}}
	<form method="POST" action="/images" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="form-group">
			<input type="file" name="image">
		</div>
		<div class="form-group">
			<button type="submit">publish a file</button>
		</div>
	</form>
	
	@include('layouts.errors')

@endsection
