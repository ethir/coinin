    @extends('layouts.main')
    @section('title')
      Authentication
    @endsection

    @section('content')
      <div class="container">
        <div class="row">
          @include("docs.sidebar")
          <div class="col-8" style="max-height: calc(100vh - 90px); overflow-y: scroll;">
            <h1>Authentication</h1>
            	<br>
	            <div data-spy="scroll" data-target="#sidebar" data-offset="0">
	            	<h3 id="get_register">get_register('/posts')</h3>
					<p>
						<b>RegistrationController@create</b> - отдаст форму для регистрации в html
					</p>
					<hr>

	            	<h3 id="post_register">post('/register')</h3>
					<p>
						<b>RegistrationController@store</b> - регистрация пользователя
						Поля:
						<code>
							[
					            'name' => (str) 'required',
					            'email' => (str) 'required',
					            'password' => (str) 'required',
					            'password_confirmation' => (str) 'required',
					        ]
				        </code> 
					</p>
					<hr>

	            	<h3 id="get_login">get('/login')</h3>
					<p>
						<b>SessionsController@create</b> - вернет форму авторизации 
					</p>
					<hr>

	            	<h3 id="post_login">post('/login')</h3>
					<p>
						<b>SessionsController@store</b> - открытие сессии, поля: 
						<code>
							[
								'email' => (str) 'required',
								'password' => (str) 'required'
							]
						</code>
					</p>
					<hr>

	            	<h3 id="get_logout">get('/logout')</h3>
	            	<p>
	            		<b>SessionsController@destroy</b> - Закрытие сессии	
	            	</p>
	            	
					<hr>

				</div>
          </div>  
        </div>
      </div>
    @endsection
