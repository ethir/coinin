<div class="col-4">
	<h1>Documentation</h1>
	<ul class="list-group" id="sidebar">
		<li class="list-group-item">
			<a href="/docs/post">Posts</a>
			<ul class="nav flex-column nav-pills">
				<li class="nav-item"><a class="nav-link" href="/docs/post/#get_posts">get('/posts')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/post/#post_posts">post('/posts')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/post/#get_posts_id">get('/posts/{id}/')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/post/#get_posts_comments">get('/posts/{id}/comments')</a></li> 
				<li class="nav-item"><a class="nav-link" href="/docs/post/#post_posts_comments">post('/posts/{id}/comments')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/post/#post_posts_comments">post('/posts/{id}/comments')</a></li>
			</ul>
		</li>
		<li class="list-group-item">
			<a href="/docs/auth">Authentication</a>
			<ul class="nav flex-column nav-pills">
				<li class="nav-item"><a class="nav-link" href="/docs/auth/#get_register">get('/register')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/auth/#post_register">post('/register')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/auth/#get_login">get('/login')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/auth/#post_login">post('/login')</a></li>
				<li class="nav-item"><a class="nav-link" href="/docs/auth/#get_logout">get('/logout')</a></li>
			</ul>
		</li>
		<li class="list-group-item">
			<a href="/docs/post">Files</a>
			<ul class="nav flex-column nav-pills">
				<li class="nav-item"><a class="nav-link" href="/docs/files/#images">post('/images')</a></li>
			</ul>
		</li>
	</ul>
</div>
