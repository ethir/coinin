    @extends('layouts.main')
    @section('title')
      Post
    @endsection

    @section('content')
      <div class="container">
        <div class="row">
          @include("docs.sidebar")
          <div class="col-8" style="max-height: calc(100vh - 90px); overflow-y: scroll;">
            <h1>Post</h1>
            	<br>
	            <div data-spy="scroll" data-target="#sidebar" data-offset="0">
	            	<h3 id="get_posts">get('/posts')</h3>
					<p>
						<b>PostsController@index</b> - отдаст список последних постов
					</p>
					<hr>

	            	<h3 id="post_posts">post('/posts')</h3>
					<p>
						<b>PostsController@store</b> - создание поста.
						Поля:
						<quote>
						<code>
							[
					            'title' => (str) 'required',
					            'body' => (text) 'required',
					            'tags' => (str) '',
					            'image' => (file) ''
					        ]
				        </code> 
					</p>
					<hr>

	            	<h3 id="get_posts_id">get('/posts/{id}/')</h3>
					<p>
						<b>PostsController@show</b> - вернет содержимое определенного поста. 
					</p>
					<hr>

	            	<h3 id="get_posts_id">get('/posts/{id}/comments')</h3>
					<p>
						<b>CommentsController@show</b> - вернет список комментариев определенного поста. 
					</p>
					<hr>
					
	            	<h3 id="post_posts_id">post('/posts/{id}/comments')</h3>
					<p>
						<b>CommentsController@store</b> - Добавление комментария, поля - <code>['body' => 'required|min:20']</code>
					</p>
					<hr>

				</div>
          </div>  
        </div>
      </div>
    @endsection
