    @extends('layouts.main')
    @section('title')
      Documentation
    @endsection

    @section('content')
      <div class="container-fluid">
        <div class="row">
          @include("docs.sidebar")
          <div class="col-8">
            <h2>Documentation</h2>
            
            Попробую держать тут общие описания роутов/классов/методов, заметки и т.д.
            <br>
            <br>
            <br>
            <br>

            <h2>
              CRUD - Create, read, update and delete
            </h2>
            
            <br>
            <br>
            
              <table class="table">
                <tr>
                  <th>Operation</th>
                  <th>SQL</th>
                  <th>HTTP</th>
                  <th>DDS</th>
                </tr>
                <tr>
                  <td>Create</td>
                  <td>INSERT</td>
                  <td>PUT / POST</td>
                  <td>write</td>
                </tr>
                <tr>
                  <td>Read (Retrieve)</td>
                  <td>SELECT</td>
                  <td>GET</td>
                  <td>read / take</td>
                </tr>
                <tr>
                  <td>Update (Modify)</td>
                  <td>UPDATE</td>
                  <td>PUT / POST / PATCH</td>
                  <td>write</td>
                </tr>
                <tr>
                  <td>Delete (Destroy)</td>
                  <td>DELETE</td>
                  <td>DELETE</td>
                  <td>dispose</td>
                </tr>
              </table>              
          
          </div>
        </div>
      </div>
    @endsection