    @extends('layouts.main')
    @section('title')
    	Files
    @endsection

    @section('content')
      <div class="container">
        <div class="row">
          @include("docs.sidebar")
          <div class="col-8" style="max-height: calc(100vh - 90px); overflow-y: scroll;">
            <h1>Files</h1>
            	<br>
	            <div data-spy="scroll" data-target="#sidebar" data-offset="0">
	            	<h3 id="post_images">post('/images')</h3>
					<p>
						<b>UploadedFile@store</b> - Сохраняет файл в директорию /images и возвращает путь
					</p>
					<hr>
          </div>  
        </div>
      </div>
    @endsection
