      <div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
        <div class="container">
           <a href="../" class="navbar-brand comfortaa">Coinin</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="/ui">UI kit</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/docs">Docs</a>
              </li>
            </ul>

            <ul class="nav navbar-nav ml-auto">
              <li class="nav-item">
              @if (Auth::check())
                <a class="nav-link" href="/logout">{{  Auth::user()->name }}</a>
              @else
                <a class="nav-link" href="/login">Login</a>
              @endif
              </li>
              @if (!Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="/register">Register</a>
                </li>
              @endif
            </ul>

          </div>
        </div>
      </div>
