<?php

use Illuminate\Database\Seeder;
// use Faker\Generator as Faker;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// $posts = factory('App\Post', 50)->create();
		$posts = App\Post::latest()->get();
		
		$posts->each(function($post){
			//TODO: Это кривовато, потом переделать
			$faker = Faker\Factory::create();
			$post->tag($faker->word);
		});
    }
}
