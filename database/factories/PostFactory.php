<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Post::class, function (Faker $faker) {
    return [
	    'title' => $faker->sentence,
        'body' => $faker->paragraph,
        'image' => $faker->imageUrl($width = 640, $height = 480, 'cats'),
        'user_id' => factory('App\User')->create()->id,
    ];
});
