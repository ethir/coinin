<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

// Переназначаем класс со сбросом защиты полей
class Model extends Eloquent
{
    // protected $fillable = ['title', 'body'];
    protected $guarded = [];
    
}
