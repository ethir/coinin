<?php

namespace App;

class Post extends Model
{
    use \Conner\Tagging\Taggable;
    
     public function comments()
     {
       return $this->hasMany(Comment::class);
     }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

     public function addComment($body)
    {
    	// 1
        // Comment::create([
        //     'body' => $body,
        //     'post_id' => $this->id
        // ]);

        // 2 
       $this->comments()->create(compact('body'));        
    }

    // Пока непонятно как возвращать теги
    // public function tags(){
        // foreach ($this->tags as $tag) {
        //     if(isset($tag)){
        //         $tagsArray[] = [ 
        //             'slug' => $tag->slug,
        //             'name' => $tag->name
        //         ];

        //     }
        // }
        // return $this->tagNames();
    // }

}
