<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Conner\Tagging\Model\Tag;

use App\Post;

class PostsController extends Controller
{  
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show', 'tags', 'byTag');
    }

    public function index()
    {   
    	$posts = Post::latest()->get();
        foreach ($posts as &$post) {
            $post['tags'] = $post->tagNames();
        }
        // die();
        // return view('posts.index', compact('posts'));
    	return $posts;
    }

    public function show(Post $post)
    {    
        return $post;
        // return view('posts.show', compact('post'));
    }

    public function create()
     {
        return view('posts.create');
     }

    public function store()
    {        
        // Create a new post using request data
        
        // 1. 
        // $post = new Post;
        // $post->title = request('postTitle');
        // $post->body = request('postContent');
        // // Save it ti the database
        // $post->save();

        // 2. 
        
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
        // Post::create([
        //     'title' => request('postTitle'),
        //     'body' => request('postContent'),
        //     'tags' => request('tags'),
        //     'image' =>
        // ]);

        // 3. 

        // $this->validate(request(), [
        //     'title' => 'required',
        //     'body' => 'required'
        // ]);

        // ->image(request()->file('image'))
        $post = new Post(request(['title', 'body']));
        if (!empty($image = request()->file('image'))) {
            $post->image = $image->store('images');
        } 

        auth()->user()->publish($post);

        $tags = array_filter(request('tags'));
        if (!empty($tags)) {
            $post->tag(request('tags'));
        }

        // Post::create([
        //     'title' => request('title'),
        //     'body' => request('body'), 
        //     'user_id' => auth()->id()
        // ]);

        // And then redirect to the home page.
        return redirect('/');
    }

    /**
     * Getting top tags of all time baby
     * 
     * @return object \Conner\Tagging\Model\Tag 
     */
    public function tags()
    {
        $tags = \Conner\Tagging\Model\Tag::orderBy('count', 'desc')
               ->take(20)
               ->get();
               
        return $tags;
    }

    /**
     * Getting posts by tag
     * @return object Post 
     */
    public function byTag($tag)
    {

        $posts = Post::withAnyTag($tag)->get();

        foreach ($posts as &$post) {
            $post['tags'] = $post->tagNames();
        }
        
        return $posts;
    }
}







