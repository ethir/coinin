<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrationController extends Controller
{
	
	public function create()
    {
    	return view('registration.create');
    }


    public function store()
    {
    	// Validate the form
    	$this->validate(request(), [
    		'name' => 'required',
    		'email' => 'required|email',
    		'password' => 'required|confirmed'
    	]);
    	// Create and save
    	$user = \App\User::create(
    		array_merge(request(['name', 'email']), array('password' => bcrypt(request('password'))) )
    	);
    	\Auth::login($user);
    	
    	// Redirect to the home
    	return redirect()->home();

    }
}
