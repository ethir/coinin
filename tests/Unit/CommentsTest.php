<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class CommentsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test **/
    function it_has_an_owner()
    {
        $comment = factory('App\Comment')->create();
        $this->assertInstanceOf('App\User', $comment->user);
    }
}
