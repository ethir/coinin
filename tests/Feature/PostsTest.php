<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PostsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->post = factory('App\Post')->create();
    }

    /** @test */
    public function a_user_can_browse_posts()
    {
        $this->get('/posts')
            ->assertSee($this->post->title);
    }

    /** @test **/
    public function a_user_can_read_a_single_post(){
        $this->get('/posts/' . $this->post->id)
            ->assertSee($this->post->title);
    }


    /** @test **/
    public function a_user_can_read_comments_that_are_associated_with_a_post(){
        $comment = factory('App\Comment')->create(['post_id' => $this->post->id]);


        print_r($comment);
        $this->get('/posts/' . $this->post->id . '/comments')
            ->assertSee($comment->body);
    }
}
