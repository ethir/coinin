<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/ui', function () {
    return view('ui');
});

/**
 *  Routes for posts
 */
Route::get('/posts/create', 'PostsController@create')->middleware('auth');

/**
 *   Routes for tags
 */
// Route::get('/tags', 'PostsController@tags');

/**
 * Routes for docs
 */
Route::get('/docs', function(){
	return view('docs.index');
});
Route::get('/docs/{doc}', function($doc){
	return view('docs.'.$doc);
});



/**
 * Routes for autorizatio
 */
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@create')->name('login');
// Route::post('/login', 'SessionsController@store');
// Route::get('/logout', 'SessionsController@destroy');



/**
 * Routes for file management
 */
Route::post('/images', function(){

	$file = request()->file('image')->store('images');
	
	return $file;
});

/**
 * 
 	RESTful

	posts
	
	GET /posts            -   получим список
	GET /posts/create     -   получим форму добавления
	POST /posts           -   добавим пост
	GET /posts/{id}/edit  -   получим форму редактирования
	GET /posts/{id}       -   получим пост для просмотра 
	PATCH /posts/{id}     -   изменим пост
	DELETE /posts/{id}    -   удалим

 */


