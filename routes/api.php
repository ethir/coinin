<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 *  Routes for posts 
 */
Route::get('/posts', 'PostsController@index');
Route::post('/posts', 'PostsController@store');
Route::get('/posts/{post}', 'PostsController@show');

Route::get('/posts/{post}/comments', 'CommentsController@show');
Route::post('/posts/{post}/comments', 'CommentsController@store');

// get posts by tag
Route::get('/posts/tag/{tag}', 'PostsController@byTag');

/**
 * Routes for tags 
 * Now using PostsController,
 * Maybe we should change it to dedicated TagsController in future
 */
// Route::get('/tags', 'PostsController@tags');

Route::get('/tags', 'PostsController@tags');

/**
 *  Routes for auth 
 */
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');





